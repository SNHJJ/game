package com.game.Controllers;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.game.Service.GameService;

@RestController
@RequestMapping("/api")
public class XmlController {

    @Autowired
    private GameService gameservice;
    

    @PostMapping(value = "/games", consumes = "SLOT/xml")
    public ResponseEntity<Object> saveGame(@RequestBody String xmlData) throws JAXBException, IOException {

        Object gameexplorer = gameservice.XmltoJava(xmlData);
        // System.out.println("\n\n"+gameexplorer);
        // return ResponseEntity.ok(gameexplorer);
        return new ResponseEntity<Object>(gameexplorer, HttpStatus.OK);
    }

}

